<?php
/**
 * The Home template file
 *
 * It should take priority over index.php and be used as the template for the home page.
 * 
 * 
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package First
 */

get_header();
?>

<div class="container-lg home-subtitle">
    <h2>Keep up with what I've been up to</h2>
</div>
<main id="main" class="home-site-main">
	<!-- <div class="container"> -->
		<driv class="grid">
	<?php
	if ( have_posts() ) :

		if ( is_home()) :
			?>
			<header>
				<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
			</header>
			<?php
		endif;

		/* Start the Loop */
		while ( have_posts() ) :
			the_post(); ?>


			<?php
			/*
			* Include the Post-Type-specific template for the content.
			* If you want to override this in a child theme, then include a file
			* called content-___.php (where ___ is the Post Type name) and that will be used instead.
			*/
			get_template_part( 'template-parts/content-home', get_post_type() );

		endwhile;

		the_posts_navigation();

	else :

		get_template_part( 'template-parts/content-home', 'none' );

	endif;
	?>
		</driv>
	<!-- </div> -->
</main><!-- #main -->
<?php
// get_sidebar();
get_footer();
