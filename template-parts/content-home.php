<?php
/**
 * Template part for displaying posts on the homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package First
 */
$tags = get_the_tags();
//	$html = '<p>Tags:</p>';
	$html = '';
 if ($tags) {
		foreach ( $tags as $tag ) {
		if($tag->slug != "migliori"){
		$tag_link = get_tag_link( $tag->term_id );
		$html .= "<span class='{$tag->slug} tag'>";
		$html .= "{$tag->name}</span>";
		// $html .= "<span><a href='{$tag_link}' class='{$tag->slug} tag'>";
		// $html .= "{$tag->name}</a></span>";

				}
		}
	}
//	$html .= '</p>';

?>
<article id="home-post-<?php the_ID(); ?>" class="home-hentry"<?php post_class(); ?>>
<div class="container">
    <!-- <?php first_post_thumbnail(); ?> -->
     <div class="tags">
    <?php if ($tags) {echo $html;} ?>
    </div>
    <?php
    if ( is_singular() ) :
        the_title( '<h1>', '</h1>' );
    else :
        the_title( '<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
    endif; ?>

    <p class="excerpt"><?php echo  get_the_excerpt(); ?></p> 
    <?php
    wp_link_pages( array(
        'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'first' ),
        'after'  => '</div>',
    ) );
    ?>
    
<footer class="home-entry-footer">
   
        
        <?php the_date(); ?><?php // comments_number( 'no responses', 'one response', '% responses' ); ?>

</footer><!-- .entry-footer -->
</div>
</article><!-- #post-<?php the_ID(); ?> -->
