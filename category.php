<?php
/**
 * The template for displaying catagory pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package First
 */

get_header();
?>
<?php if ( have_posts() ) : ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<!-- <div class="container"> -->
			<div class="grid">
				

					

					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
						* Include the Post-Type-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Type name) and that will be used instead.
						*/
						get_template_part( 'template-parts/content-home', get_post_type() );

					endwhile;

					the_posts_navigation();

				else :

					get_template_part( 'template-parts/content-home', 'none' );

				endif;
				?>
			</div>
		<!-- </div> -->
	</main><!-- #main -->
</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
