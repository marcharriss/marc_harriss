console.log('test');

document.body.addEventListener('click', function(e) {
    var t = e.target;
    if (t.href) {
        ga('send', {
            hitType: 'event',
            eventCategory: 'link',
            eventAction: window.location.href,
            eventLabel: 'Navigation',
            transport: 'beacon'
        });
        
    }

    if (t.hasAttribute('data')) {
        ga('send', {
            hitType: 'event',
            eventCategory: 'all',
            eventAction: t.getAttribute('data'),
            eventLabel: 'all',
        });
        console.log(t.getAttribute('data'), t.attributes.class);
        console.log(e);
    }

   
}, false);

document.body.addEventListener('copy', function (e) {
   
    ga('send', {
        hitType: 'event',
        eventCategory: 'Copy',
        eventAction: document.getSelection().toString(),
        eventLabel: 'TextCopy',
    });


}, false);

// var copy = document.createElement('i'); 
// copy.classList.add('fas');
// copy.classList.add('fa-copy');
// copy.classList.add('copy');
// copy.innerHTML = 'test';
// console.log(copy);

// var codeBlocks = document.querySelectorAll('pre > code');
// codeBlocks.forEach(function(el) {
//     el.appendChild(copy);
//     console.log(el);
// })