<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package First
 */
?>

<?php
	$tags = get_the_tags();
//	$html = '<p>Tags:</p>';
	$html = '';
	if ($tags) {
		foreach ( $tags as $tag ) {
		if($tag->slug != "migliori"){
		$tag_link = get_tag_link( $tag->term_id );
		$html .= "<span class='{$tag->slug} tag'>";
		$html .= "{$tag->name}</span>";
		// $html .= "<span><a href='{$tag_link}' class='{$tag->slug} tag'>";
		// $html .= "{$tag->name}</a></span>";

				}
		}
	}
//	$html .= '</p>';
?>  
<div id="progress"></div>
<div class="post-container">



			<!-- <header class="entry-header"> -->
				<?php /*
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title" data="h1-link-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif; */
				?>
			<!-- </header>.entry-header -->

			<!-- <div class="tags">
				<?php // if ($tags) {echo $html;} ?>
			</div>
			<hr> -->

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		

		<div class="entry-content">
			<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'first' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );
				?>
		</div><!-- entry-content -->

		<footer class="entry-footer">
			<?php first_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	</article><!-- #post-<?php the_ID(); ?> -->

