<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package First
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<!-- <style>.async-hide { opacity: 0 !important} </style> -->
<!-- <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-WNMV9CD':true});</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-118754587-1', 'auto');
  ga('require', 'GTM-WNMV9CD');
  ga('send', 'pageview');

// Google Tag Manager
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P74BV7N');
// End Google Tag Manager
</script> -->
<script type="application/javascript">
  (function(b, o, n, g, s, r, c) { if (b[s]) return; b[s] = {}; b[s].scriptToken = "Xy0xNDAwNjM0MDM"; r = o.createElement(n); c = o.getElementsByTagName(n)[0]; r.async = 1; r.src = g; r.id = s + n; c.parentNode.insertBefore(r, c); })(window, document, "script", "//cdn.oribi.io/Xy0xNDAwNjM0MDM/oribi.js", "ORIBI");
</script>
<script>
window['_fs_debug'] = false;
window['_fs_host'] = 'fullstory.com';
window['_fs_org'] = 'FG7XM';
window['_fs_namespace'] = 'FS';
(function(m,n,e,t,l,o,g,y){
    if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
    g=m[e]=function(a,b){g.q?g.q.push([a,b]):g._api(a,b);};g.q=[];
    o=n.createElement(t);o.async=1;o.src='https://'+_fs_host+'/s/fs.js';
    y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
    g.identify=function(i,v){g(l,{uid:i});if(v)g(l,v)};g.setUserVars=function(v){g(l,v)};g.event=function(i,v,s){g('event',{n:i,p:v,s:s})};
    g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)};
    g.consent=function(a){g("consent",!arguments.length||a)};
    g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
    g.clearUserCookie=function(){};
})(window,document,window['_fs_namespace'],'script','user');
</script>
<!-- Global site tag (gtag.js) - Google Analytics
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118754587-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118754587-1');
</script> -->

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' )?>/stylesheets/style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
	<?php wp_head(); ?> 
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P74BV7N"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'first' ); ?></a>
	<button class="menu-toggle"><i class="fas fa-bars"></i></button>
	<nav id="site-navigation" class="main-navigation">
				
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
				?>
	</nav><!-- #site-navigation -->
	



				<?php
				if ( is_front_page() && is_home() ) :
					?>
					<header id="masthead" class="site-header">
					<div class="header-container">
						<div class="site-branding">
							<h1 class="site-title"><a href="<?php bloginfo( 'url' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
					$first_description = get_bloginfo( 'description', 'display' );
				if ( $first_description || is_customize_preview() ) :
					?>
					<p class="site-description"><?php echo $first_description; /* WPCS: xss ok. */ ?></p>
					<?php endif; ?>

					<div class="social-links">
					<a href="https://github.com/MarcHarrisscoding" target="_blank" class="github"><i class="fab fa-github-square github"></i>Github</a>
					<a href="https://www.linkedin.com/in/marcharriss/" target="_blank" class="linkedin"><i class="fab fa-linkedin linkedin"></i>LinkedIn</a>
					<a href="https://twitter.com/harriss_marc" target="_blank" class="twitter"><i class="fab fa-twitter-square twitter"></i>Twitter</a>
					</div>

					<?php 
					
					get_search_form(); 

				



				elseif ( is_category() ) : 
					$current_term = single_term_title( "", false );
					?>
					<header id="masthead" class="site-header-cat">
					<div class="header-container sm">
						<div class="tags-header">
							<h1 class="site-title center"><?php echo $current_term	?></h1>
						</div>	
					</div>	

				<?php elseif ( is_search() ) : ?>


					<header id="masthead" class="site-header-post">
					<div class="header-container sm">
						<div class="tags-header">
						<?php get_search_form(); ?>
							</div>
					</div>

				<?php else :
					$tags = get_the_tags();
					//	$html = '<p>Tags:</p>';
						$html = '';
						if ($tags) {
							foreach ( $tags as $tag ) {
							if($tag->slug != "migliori"){
							$tag_link = get_tag_link( $tag->term_id );
							$html .= "<span class='{$tag->slug} tag'>";
							$html .= "{$tag->name}</span>";
							// $html .= "<span><a href='{$tag_link}' class='{$tag->slug} tag'>";
							// $html .= "{$tag->name}</a></span>";

									}
							}
						}
					//	$html .= '</p>';
					?>
					<header id="masthead" class="site-header-post">
					<div class="header-container sm">
						<div class="tags-header">
						<h1 class="site-title center"><?php echo get_the_title(); ?></h1>
								<?php if ($tags) {echo $html;} ?>
							</div>
					</div>	
					<?php
				endif;
				?>
			</div><!-- .site-branding -->

		</div>	
	</header><!-- #masthead -->

	<div id="content" class="site-content">
