<?php

$args = array(
    'type'                     => 'category',
    'child_of'                 => 0,
    'parent'                   => 0,
    'orderby'                  => '',
    'order'                    => 'ASC',
    'hide_empty'               => 1,        // hide empty categories [0] false
    'hierarchical'             => 1,
    'exclude'                  => '',
    'include'                  => '',
    'number'                   => '',
    'taxonomy'                 => 'category',
    'pad_counts'               => false
);

$categories = get_categories( $args );




/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package First
 */

get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div class="container">
			<div class="grid">
                <?php
                    foreach($categories as $category) {
                        echo '<div class="container center">';
                        echo '<h2>' . $category->name . '</h2>';
                        echo '</div>';
                        $parent_id = $category->term_id;

                        $args2 = array(
                        'type'                     => 'category',
                        'child_of'                 => $parent_id,
                       'parent'                    => $parent_id,
                        'orderby'                  => 'name',
                        'order'                    => 'ASC',
                        'hide_empty'               => 1,        // hide empty categories [0] false
                        'hierarchical'             => 1,
                        'exclude'                  => '',
                        'include'                  => '',
                        'number'                   => '',
                        'taxonomy'                 => 'category',
                        'pad_counts'               => false
                        );
                            
                        $subcategories = get_categories( $args2 );

                        foreach($subcategories as $sub) {
                            $cat_link = get_category_link($sub->cat_ID);
                            if (function_exists('get_wp_term_image'))
                                {
                                    $meta_image = get_wp_term_image($sub->cat_ID); 
                                    //It will give category/term image url 
                                }

                                echo '<a class="subcats" href="' . $cat_link . '"><img src="' . $meta_image . '">'; // category/term image url
                            echo '<p>' . $sub->name . '</p></a>';
                        }
                    }
                ?>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
