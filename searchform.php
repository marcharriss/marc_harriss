<form role="search" method="get" class="search-form" action="https://marcharriss.com">
    <label>
        <span class="screen-reader-text">Search for:</span>
        <input type="search" class="search-field aa-input" placeholder="What are you interested in?" value="" name="s" autocomplete="off" spellcheck="false" role="combobox" aria-autocomplete="list" aria-expanded="false" aria-owns="algolia-autocomplete-listbox-0" dir="auto" style=""><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &quot;Nunito Sans&quot;, sans-serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: normal; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre>
    </label>
</form>